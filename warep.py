from datetime import datetime
import csv

# TUPLES WITH INDEX ALL BELARUSIAN'S METEOROLOGICAL-STATIONS, DIVIDING BY THREE ZONES
BY1 = (
    '26736', '26825', '26832', '26834', '26836', '26923', '26929', '26938', '26941', '26947', '33001', '33008',
    '33011', '33015', '33019'
)
BY2 = (
    '26554', '26643', '26645', '26649', '26653', '26657', '26659', '26745', '26748', '26759', '26846', '26850',
    '26853', '26855', '26951', '26959'
)
BY3 = (
    '26566', '26666', '26668', '26763', '26774', '26779', '26862', '26863', '26864', '26878', '26887', '26950',
    '26961', '26966', '33027', '33036', '33038', '33041', '33124'
)

today = datetime.utcnow().strftime('%Y-%m-%d')


def get_data_from_file():
    '''OPEN DAT FILE FOR READING AND GET ALL STORM INFORMATION BY ALL STATIONS THEN
    SAVE DATA TO LIST CONSIST OF DICT {'DATE' : DATA,
                                        'BY1 STORM' : DATA,
                                        'BY1 AWIA' : DATA,
                                        'BY2 STORM' : DATA,
                                        'BY2 AWIA' : DATA,
                                        'BY3 STORM' : DATA,
                                        'BY3 AWIA' : DATA,}'''
    with open('UMMN.DAT', 'r', encoding="ISO-8859-1") as file:
        data = []
        for line in file.readlines():
            if 'AWIA' in line or 'STORM' in line or 'WAREP' in line:
                data.append(line.replace('\n', ''))
        # DIVIDE DATA BY TYPE OF MASSAGE
        storm_data = []

        awia_data = []

        for i, v in enumerate(data):
            if v == 'STORM':
                storm_data.append(data[i + 1])
            elif v == 'AWIA':
                awia_data.append(data[i + 1])

    dict_data = {
        'Date': datetime.today().strftime('%d.%m'),
        'BY1 STORM': [],
        'BY1 AWIA': [],
        'BY2 STORM': [],
        'BY2 AWIA': [],
        'BY3 STORM': [],
        'BY3 AWIA': [],
    }

    for el in storm_data:
        if el.split(" ")[1] in BY1 or el.split(" ")[2] in BY1:
            dict_data['BY1 STORM'].append(el.replace('STORM', ''))
        elif el.split(" ")[1] in BY2 or el.split(" ")[2] in BY2:
            dict_data['BY2 STORM'].append(el.replace('STORM', ''))
        elif el.split(" ")[1] in BY3 or el.split(" ")[2] in BY3:
            dict_data['BY3 STORM'].append(el.replace('STORM', ''))

    for el in awia_data:
        if el.split(" ")[1] in BY1 or el.split(" ")[2] in BY1:
            dict_data['BY1 AWIA'].append(el.replace('STORM', ''))
        elif el.split(" ")[1] in BY2 or el.split(" ")[2] in BY2:
            dict_data['BY2 AWIA'].append(el.replace('STORM', ''))
        elif el.split(" ")[1] in BY3 or el.split(" ")[2] in BY3:
            dict_data['BY3 AWIA'].append(el.replace('STORM', ''))

    def func(ls):
        '''HELP EXECUTE DATA FROM DICT, IF LIST WITH DATA IS EMPTY'''
        try:
            return ls.pop(0)
        except IndexError:
            return ''

    i = max([len(dict_data['BY1 STORM']), len(dict_data['BY1 AWIA']), len(dict_data['BY2 STORM']), len(
        dict_data['BY2 AWIA']), len(dict_data['BY3 STORM']), len(dict_data['BY3 AWIA'])])
    global big_data
    big_data = [{'Date': dict_data['Date'],
                 'BY1 STORM': func(dict_data['BY1 STORM']),
                 'BY1 AWIA': func(dict_data['BY1 AWIA']),
                 'BY2 STORM': func(dict_data['BY2 STORM']),
                 'BY2 AWIA': func(dict_data['BY2 AWIA']),
                 'BY3 STORM': func(dict_data['BY3 STORM']),
                 'BY3 AWIA': func(dict_data['BY3 AWIA']),
                 } for _ in range(i)]


def write_data_to_file():
    '''WRITE ALL EXECUTING DATA TO FILE'''
    with open(f'{today}.csv', 'w') as file:
        fieldnames = ['Date', 'BY1 STORM', 'BY1 AWIA', 'BY2 STORM', 'BY2 AWIA', 'BY3 STORM', 'BY3 AWIA']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(big_data)


if __name__ == '__main__':
    get_data_from_file()
    write_data_to_file()

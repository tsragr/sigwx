import csv
from datetime import datetime

big_data = []

def get_data(dat_file):
    type_data = dat_file.split('.')[0]
    with open(dat_file, 'r') as met_file:
        data = met_file.readlines()

    new_data = []
    for k, v in enumerate(data):
        if type_data in v:
            new_data.append(f"{v[:-1]} {data[k + 1][:-1]}")

    for el in new_data:
        big_data.append(' '.join(el.split()[2:]))


def main():
    today = datetime.utcnow().strftime('%Y-%m-%d')

    current_data = []

    get_data('METAR.DAT')
    get_data('SPECI.DAT')
    big_data.sort()
    try:
        with open(f'{today}.csv', 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                current_data.append(row)
    except FileNotFoundError:
        with open(f'{today}.csv', 'w') as file:
            fields = ['metar/speci', 'sinop name', '%']
            writer = csv.DictWriter(file, fieldnames=fields)
            writer.writeheader()
            for el in big_data:
                writer.writerow({'metar/speci': el})

    if current_data:
        with open(f'{today}.csv', 'a') as file:
            writer = csv.DictWriter(file, fieldnames=['metar/speci', 'sinop name', '%'])
            for el in big_data[len(current_data[1:]):]:
                writer.writerow({'metar/speci': el})


if __name__ == '__main__':
    main()

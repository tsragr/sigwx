import matplotlib.colors as mcolors
import matplotlib.patheffects as mpatheffects
import metpy.interpolate as mpinterpolate
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import pandas as pd
import metpy.calc as mpcalc
from metpy.cbook import get_test_data
from metpy.plots import add_metpy_logo, Hodograph, SkewT
from metpy.units import units
from mpl_toolkits.axisartist.axislines import AxesZero
from tkinter import *


class Application(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.create_widgets()

    wind_s = []
    wind_d = []

    def create_widgets(self):
        Label(self, text='Points').grid(row=0, column=0)
        Label(self, text='Speed (m/s)').grid(row=0, column=1)
        Label(self, text='Direction (degree)').grid(row=0, column=2)
        Label(self, text=f'sfc').grid(row=1, column=0)
        self.s_one = Entry(self)
        self.s_one.grid(row=1, column=1)
        self.d_one = Entry(self)
        self.d_one.grid(row=1, column=2)
        Label(self, text=f'1').grid(row=2, column=0)
        self.s_two = Entry(self)
        self.s_two.grid(row=2, column=1)
        self.d_two = Entry(self)
        self.d_two.grid(row=2, column=2)
        Label(self, text=f'2').grid(row=3, column=0)
        self.s_three = Entry(self)
        self.s_three.grid(row=3, column=1)
        self.d_three = Entry(self)
        self.d_three.grid(row=3, column=2)
        Label(self, text=f'3').grid(row=4, column=0)
        self.s_four = Entry(self)
        self.s_four.grid(row=4, column=1)
        self.d_four = Entry(self)
        self.d_four.grid(row=4, column=2)
        Label(self, text=f'4').grid(row=5, column=0)
        self.s_five = Entry(self)
        self.s_five.grid(row=5, column=1)
        self.d_five = Entry(self)
        self.d_five.grid(row=5, column=2)
        Label(self, text=f'5').grid(row=6, column=0)
        self.s_six = Entry(self)
        self.s_six.grid(row=6, column=1)
        self.d_six = Entry(self)
        self.d_six.grid(row=6, column=2)
        Label(self, text=f'6').grid(row=7, column=0)
        self.s_seven = Entry(self)
        self.s_seven.grid(row=7, column=1)
        self.d_seven = Entry(self)
        self.d_seven.grid(row=7, column=2)
        Label(self, text=f'7').grid(row=8, column=0)
        self.s_eight = Entry(self)
        self.s_eight.grid(row=8, column=1)
        self.d_eight = Entry(self)
        self.d_eight.grid(row=8, column=2)
        Label(self, text=f'8').grid(row=9, column=0)
        self.s_nine = Entry(self)
        self.s_nine.grid(row=9, column=1)
        self.d_nine = Entry(self)
        self.d_nine.grid(row=9, column=2)
        Label(self, text=f'9').grid(row=10, column=0)
        self.s_ten = Entry(self)
        self.s_ten.grid(row=10, column=1)
        self.d_ten = Entry(self)
        self.d_ten.grid(row=10, column=2)

        Button(self, text='Create Hodograph', command=self.create_hodograph).grid(row=11, column=0, columnspan=2)

    def create_hodograph(self):

        self.wind_s.append(self.s_one.get())
        self.wind_s.append(self.s_two.get())
        self.wind_s.append(self.s_three.get())
        self.wind_s.append(self.s_four.get())
        self.wind_s.append(self.s_five.get())
        self.wind_s.append(self.s_six.get())
        self.wind_s.append(self.s_seven.get())
        self.wind_s.append(self.s_eight.get())
        self.wind_s.append(self.s_nine.get())
        self.wind_s.append(self.s_ten.get())

        self.wind_d.append(self.d_one.get())
        self.wind_d.append(self.d_two.get())
        self.wind_d.append(self.d_three.get())
        self.wind_d.append(self.d_four.get())
        self.wind_d.append(self.d_five.get())
        self.wind_d.append(self.d_six.get())
        self.wind_d.append(self.d_seven.get())
        self.wind_d.append(self.d_eight.get())
        self.wind_d.append(self.d_nine.get())
        self.wind_d.append(self.d_ten.get())
        while '' in self.wind_s and '' in self.wind_d:
            self.wind_s.remove('')
            self.wind_d.remove('')
        wind_s = list(map(int, self.wind_s))
        wind_d = list(map(int, self.wind_d))

        boundaries_h = [0, 1.5, 2, 3, 4] * units.kilometers

        wind_speed = wind_s * (units.meters / units.seconds)
        wind_dir = wind_d * units.degrees
        u, v = mpcalc.wind_components(wind_speed, wind_dir)

        #fig = plt.figure(figsize=(9, 9))

        ax = plt.figure(figsize=(9, 9)).add_subplot(axes_class=AxesZero)
        ax.axis['xzero'].set_visible(True)
        ax.axis["xzero"].label.set_text("")

        for up, vp, z in zip(u, v, list(range(len(wind_s)))):
            # z_str = '{:~.0f}'.format(z.to('km')) if z else 'Sfc'
            z = z if z != 0 else 'sfc'
            ax.text(up, vp, z, ha='center', fontsize=9,
                    path_effects=[mpatheffects.withStroke(foreground='r', linewidth=1)],
                    zorder=10)

        h = Hodograph(ax, component_range=max(wind_s) + 10.)
        h.add_grid(increment=5, )
        h.add_grid(increment=10, color='tab:orange', linestyle='-')
        h.plot(u, v, color='tab:red', label='line 1', linewidth=2)
        # Show the plot

        plt.show()
        self.wind_s.clear()
        self.wind_d.clear()

root = Tk()
root.title('Hodograph')
app = Application(root)
root.mainloop()

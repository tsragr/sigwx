import requests
from bs4 import BeautifulSoup
from PIL import Image
from io import BytesIO
from datetime import datetime , time

current_time = int(datetime.utcnow().strftime("%H"))
if current_time == 1 or current_time < 7:
    last = "ASIA_18.gif"
    last_6 = "ASIA_12.gif"
    last_12 = "ASIA_06.gif"
    last_18 = "ASIA_00.gif"
elif current_time == 7 or current_time < 13:
    last = "ASIA_00.gif"
    last_6 = "ASIA_18.gif"
    last_12 = "ASIA_12.gif"
    last_18 = "ASIA_06.gif"
elif current_time == 13 or current_time < 19:
    last = "ASIA_06.gif"
    last_6 = "ASIA_00.gif"
    last_12 = "ASIA_18.gif"
    last_18 = "ASIA_12.gif"
elif current_time == 19 or current_time < 24:
    last = "ASIA_12.gif"
    last_6 = "ASIA_06.gif"
    last_12 = "ASIA_00.gif"
    last_18 = "ASIA_18.gif"

europ_urls = {'EUROP_00.png': 'https://www.vedur.is/photos/flugkort/PGDE14_EGRR_0000.png',
              'EUROP_06.png': 'https://www.vedur.is/photos/flugkort/PGDE14_EGRR_0600.png',
              'EUROP_12.png': 'https://www.vedur.is/photos/flugkort/PGDE14_EGRR_1200.png',
              'EUROP_18.png': 'https://www.vedur.is/photos/flugkort/PGDE14_EGRR_1800.png', }


def europ():
    for k, v in europ_urls.items():
        resp = requests.get(v)
        image = Image.open(BytesIO(resp.content))
        image.save(f"/home/maksim/PycharmProjects/CodeWars/EUROP/{k}")


asia_urls = {last: 'https://aviationweather.gov/data/iffdp/2105.gif',
             last_6: 'https://aviationweather.gov/data/iffdp/3105.gif',
             last_12: 'https://aviationweather.gov/data/iffdp/4105.gif',
             last_18: 'https://aviationweather.gov/data/iffdp/5105.gif'}


def asia():
    for key, value in asia_urls.items():
        r = requests.get(value)
        image = Image.open(BytesIO(r.content))
        image.save(f"/home/maksim/PycharmProjects/CodeWars/ASIA/{key}")


if __name__ == '__main__':
    europ()
    asia()

